import UIKit

class UITableViewVibrantCell: UIViewController {
    
    @IBOutlet var buttons : [UIButton]!
    
    @IBOutlet weak var caspianBtn: UIButton!
    @IBOutlet weak var marinerBtn: UIButton!
    @IBOutlet weak var badOneBtn: UIButton!
    @IBOutlet weak var badTwoBtn: UIButton!
    @IBOutlet weak var galleryBtn: UIButton!
    @IBOutlet weak var badThreeBtn: UIButton!
    
    
    @IBOutlet weak var sandpiper: UIImageView!
    @IBOutlet weak var voyager: UIImageView!
    @IBOutlet weak var clipper: UIImageView!
    @IBOutlet weak var mariner: UIImageView!
    @IBOutlet weak var caspian: UIImageView!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBAction func bad1btn(_ sender: UIButton) {
        clipper.isHidden = true
        badThreeBtn.isSelected = false;
        mariner.isHidden = true
        marinerBtn.isSelected = false;
        caspian.isHidden = true
        caspianBtn.isSelected = false;
        
        badOneBtn.isSelected = !badOneBtn.isSelected
        if badOneBtn.isSelected {
            sandpiper.isHidden = false
        } else {
            sandpiper.isHidden = true
            
            if (self.badTwoBtn.isSelected) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationBadTwo"),object: nil, userInfo: ["selected" : self.badTwoBtn.isSelected, "1BR" : false])
                return
            }
        }
        
        let sandpiperAndVoyager = self.firstBR()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationBadOne"), object: nil, userInfo: ["selected" : self.badOneBtn.isSelected, "1BR" : sandpiperAndVoyager])
    }
    
    func firstBR() -> (Bool) {
        return badOneBtn.isSelected && badTwoBtn.isSelected
    }
    
    func secondBR() -> (Bool) {
        return badThreeBtn.isSelected && marinerBtn.isSelected && caspianBtn.isSelected
    }
    
    @IBAction func bad2btn(_ sender: UIButton) {
        clipper.isHidden = true
        badThreeBtn.isSelected = false;
        mariner.isHidden = true
        marinerBtn.isSelected = false;
        caspian.isHidden = true
        caspianBtn.isSelected = false;
        
        badTwoBtn.isSelected = !badTwoBtn.isSelected
        if badTwoBtn.isSelected {
            voyager.isHidden = false
        } else {
            voyager.isHidden = true
            
            if (self.badOneBtn.isSelected) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationBadOne"),object: nil, userInfo: ["selected" : self.badOneBtn.isSelected, "1BR" : false])
                return
            }
        }
        
        let sandpiperAndVoyager = self.firstBR()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationBadTwo"),object: nil, userInfo: ["selected" : self.badTwoBtn.isSelected, "1BR" : sandpiperAndVoyager])
    }
    
    @IBAction func bad3btn(_ sender: UIButton) {
        clearButtons(sender: sender)
        badThreeBtn.isSelected = !badThreeBtn.isSelected
        if badThreeBtn.isSelected {
            clipper.isHidden = false
        } else {
            clipper.isHidden = true
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationBadThree"),object: nil, userInfo: ["selected" : self.badThreeBtn.isSelected, "marinerSelected": marinerBtn.isSelected, "caspianSelected" : caspianBtn.isSelected])
    }
    
    @IBAction func marinerBtn(_ sender: UIButton) {
        clearButtons(sender: sender)
        marinerBtn.isSelected = !marinerBtn.isSelected
        if marinerBtn.isSelected {
            mariner.isHidden = false
        } else {
            mariner.isHidden = true
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationMariner"),object: nil, userInfo: ["selected" : self.marinerBtn.isSelected, "clipperSelected": badThreeBtn.isSelected, "caspianSelected":caspianBtn.isSelected])
    }
    
    @IBAction func caspianBtn(_ sender: UIButton) {
        clearButtons(sender: sender)
        
        caspianBtn.isSelected = !caspianBtn.isSelected
        if caspianBtn.isSelected {
            caspian.isHidden = false
        } else {
            caspian.isHidden = true
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationCaspian"),object: nil, userInfo: ["selected" : self.caspianBtn.isSelected, "marinerSelected": marinerBtn.isSelected, "clipperSelected": badThreeBtn.isSelected])
    }
    
    func clearButtons (sender: UIButton!) {
        sandpiper.isHidden = true
        badOneBtn.isSelected = false
        voyager.isHidden = true
        badTwoBtn.isSelected = false
    }
   
    @IBAction func amenitiesBtn(_ sender: UIButton) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationBadAmenities"),object: nil)
        }

    }
    @IBAction func galleryBtn(_ sender: UIButton) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationBadGallery"),object: nil)
        }
    }
    @IBAction func plainBtn(_ sender: UIButton) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kNotificationBadNewGallery"),object: nil)
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        badOneBtn.isSelected = UserDefaults.standard.bool(forKey: "Sandpiper")
        if (badOneBtn.isSelected) {
            sandpiper.isHidden = false
        }
        
        badTwoBtn.isSelected = UserDefaults.standard.bool(forKey: "Voyager")
        if (badTwoBtn.isSelected) {
            voyager.isHidden = false
        }
        
        badThreeBtn.isSelected = UserDefaults.standard.bool(forKey: "Clipper")
        if (badThreeBtn.isSelected) {
            clipper.isHidden = false
        }
        
        marinerBtn.isSelected = UserDefaults.standard.bool(forKey: "Mariner")
        if (marinerBtn.isSelected) {
            mariner.isHidden = false
        }
        
        caspianBtn.isSelected = UserDefaults.standard.bool(forKey: "Caspian")
        if (caspianBtn.isSelected) {
            caspian.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(badOneBtn.isSelected, forKey: "Sandpiper")
        UserDefaults.standard.set(badTwoBtn.isSelected, forKey: "Voyager")
        UserDefaults.standard.set(badThreeBtn.isSelected, forKey: "Clipper")
        UserDefaults.standard.set(marinerBtn.isSelected, forKey: "Mariner")
        UserDefaults.standard.set(caspianBtn.isSelected, forKey: "Caspian")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        topConstraint.constant = UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft || UIDevice.current.orientation == UIDeviceOrientation.landscapeRight ? 130 : 200
        self.view.updateConstraints()
        bottomConstraint.constant = UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft || UIDevice.current.orientation == UIDeviceOrientation.landscapeRight ? 50 : 150
        self.view.updateConstraints()


    }
}
