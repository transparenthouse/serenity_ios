//
//  ViewController.swift
//  SerenitySiteMap
//
//  Created by Ilya on 29/08/16.
//  Copyright © 2016 DDG LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
     
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var changedImageView: UIImageView!
    @IBOutlet weak var strokeImageView: UIImageView!
    
    @IBOutlet weak var secondLevelImage: UIImageView!
    @IBOutlet weak var thirdLevelImage: UIImageView!
    
    @IBOutlet weak var buttonWidth: NSLayoutConstraint!
    
    var loadMainView: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(pressBtnOne), name: NSNotification.Name(rawValue: "kNotificationBadOne"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pressBtnTwo), name: NSNotification.Name(rawValue: "kNotificationBadTwo"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pressBtnThree), name: NSNotification.Name(rawValue: "kNotificationBadThree"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pressBtnGallery), name: NSNotification.Name(rawValue: "kNotificationBadGallery"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pressMarinerBtn), name: NSNotification.Name(rawValue: "kNotificationMariner"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pressCaspianBtn), name: NSNotification.Name(rawValue: "kNotificationCaspian"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pressAmet), name: NSNotification.Name(rawValue: "kNotificationBadAmenities"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pressNewGallery), name: NSNotification.Name(rawValue: "kNotificationBadNewGallery"), object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (!loadMainView) {
            loadMainView = true
            self.mainImageView.image = UIImage(named: "Main")
            self.strokeImageView.image = UIImage(named: "Stroke")
            self.changedImageView.image = UIImage(named: "Text")
        }
    }
    
    public func pressBtnOne(notification: Notification) {
        clearImageAnimated(imageView: secondLevelImage)
        clearImageAnimated(imageView: thirdLevelImage)
        //self.secondLevelImage.image = nil
        //self.thirdLevelImage.image = nil
        
        let selected: NSNumber = notification.userInfo?["selected"] as! NSNumber
        let firstBR: NSNumber = notification.userInfo?["1BR"] as! NSNumber
        let name = firstBR.boolValue ? "SandpiperVoyager" : "Sandpiper"
        self.setImageWithName(name: name, selected: selected.boolValue)
    }
    
    func setImageWithName(name: String, selected: Bool) {
        if selected {
            combineImagesInBackground(fromIcon: name, toImage: self.mainImageView)
        } else {
            self.defaultImage()
        }
    }
    
    func defaultImage() {
        combineImagesInBackground(fromIcon: "Text", toImage: self.mainImageView)
    }
    
    func pressBtnTwo(notification: Notification) {
        clearImageAnimated(imageView: secondLevelImage)
        clearImageAnimated(imageView: thirdLevelImage)
        //self.secondLevelImage.image = nil
        //self.thirdLevelImage.image = nil
        
        let selected: NSNumber = notification.userInfo?["selected"] as! NSNumber
        let firstBR: NSNumber = notification.userInfo?["1BR"] as! NSNumber
        let name = firstBR.boolValue ? "SandpiperVoyager" : "Voyager"
        self.setImageWithName(name: name, selected: selected.boolValue)
    }
    
    func pressBtnThree(notification: Notification) {
        let selected: NSNumber = notification.userInfo?["selected"] as! NSNumber
        let mariner: NSNumber = notification.userInfo?["marinerSelected"] as! NSNumber
        let caspian: NSNumber = notification.userInfo?["caspianSelected"] as! NSNumber
        
        if (selected.boolValue) {
            changeImageAnimated(imageName: "Clipper", imageView: changedImageView)
            //self.changedImageView.image = UIImage(named: "Clipper")
        } else if (!mariner.boolValue && !caspian.boolValue) {
            changeImageAnimated(imageName: "Text", imageView: changedImageView)
            //self.changedImageView.image = UIImage(named: "Text")
        } else {
            //self.changedImageView.image = nil
            clearImageAnimated(imageView: changedImageView)
        }
    }
    
    func pressMarinerBtn(notification: Notification) {
        let selected: NSNumber = notification.userInfo?["selected"] as! NSNumber
        let clipper: NSNumber = notification.userInfo?["clipperSelected"] as! NSNumber
        let caspian: NSNumber = notification.userInfo?["caspianSelected"] as! NSNumber
        
        if (!clipper.boolValue) {
            clearImageAnimated(imageView: changedImageView)
            //self.changedImageView.image = nil;
        }
        
        if (selected.boolValue) {
            changeImageAnimated(imageName: "Mariner", imageView: secondLevelImage)
            //self.secondLevelImage.image = UIImage(named: "Mariner")
        } else if (!clipper.boolValue && !caspian.boolValue) {
            changeImageAnimated(imageName: "Text", imageView: changedImageView)
            //self.changedImageView.image = UIImage(named: "Text")
            clearImageAnimated(imageView: secondLevelImage)
            //self.secondLevelImage.image = nil
        } else {
            clearImageAnimated(imageView: secondLevelImage)
            //self.secondLevelImage.image = nil
        }
    }
    
    func pressCaspianBtn(notification: Notification) {
        let selected: NSNumber = notification.userInfo?["selected"] as! NSNumber
        let mariner: NSNumber = notification.userInfo?["marinerSelected"] as! NSNumber
        let clipper: NSNumber = notification.userInfo?["clipperSelected"] as! NSNumber
        
        if (!clipper.boolValue) {
            clearImageAnimated(imageView: changedImageView)
            //self.changedImageView.image = nil;
        }
        
        if (selected.boolValue) {
            self.changeImageAnimated(imageName: "Caspian", imageView: thirdLevelImage)
            //self.thirdLevelImage.image = UIImage(named: "Caspian")
        } else if(!mariner.boolValue && !clipper.boolValue) {
            self.changeImageAnimated(imageName: "Text", imageView: changedImageView)
            //self.changedImageView.image = UIImage(named: "Text")
            clearImageAnimated(imageView: thirdLevelImage)
            //self.thirdLevelImage.image = nil;
        } else {
            clearImageAnimated(imageView: thirdLevelImage)
            //self.thirdLevelImage.image = nil;
        }
    }
    
    func changeImageAnimated(imageName: String, imageView: UIImageView) {
        let image = UIImage(named: imageName)
        UIView.transition(with: imageView, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            imageView.image = image
            }, completion: nil)
    }
    
    func clearImageAnimated(imageView: UIImageView) {
        UIView.transition(with: imageView, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            imageView.image = nil
            }, completion: nil)
    }
    
    func addNumbers() {
        combineImagesInBackground(fromIcon: "Text", toImage: self.mainImageView)
    }
    
    func pressBtnGallery() {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ParentController") as? ParentController
        mapViewControllerObj?.identifier = "Gallery"
        self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
    }
    
    
    func pressAmet() {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ParentController") as? ParentController
        mapViewControllerObj?.identifier = "Amenities"
        self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
    }
    
    func pressNewGallery() {
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ParentController") as? ParentController
        mapViewControllerObj?.identifier = "NewGallery"
        self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
    }

    
    func combineImagesInBackground(fromIcon icon: String, toImage imageView : UIImageView) {
        let toImage = UIImage(named: icon)!
        UIView.transition(with: changedImageView, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
            self.changedImageView.image = toImage
            }, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 4.0
        self.scrollView.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.tapAction(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.containerView.isUserInteractionEnabled = true
        self.containerView.addGestureRecognizer(tapGestureRecognizer)
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.containerView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if (scrollView.zoomScale < 1) {
            scrollView.zoomScale = 1
        }
    }
    
    private func updateMinZoomScaleForSize(size: CGSize) {
        let widthScale = size.width / self.containerView.bounds.width
        let heightScale = size.height / self.containerView.bounds.height
        let minScale = min(widthScale, heightScale)
        self.scrollView.minimumZoomScale = minScale
        self.scrollView.zoomScale = minScale
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if (!loadMainView) {
            updateMinZoomScaleForSize(size: view.bounds.size)
        }
        buttonWidth.constant = UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft || UIDevice.current.orientation == UIDeviceOrientation.landscapeRight ? 37 : 50
        self.view.updateConstraints()
        print("\(buttonWidth.constant)")
    }
    
    func tapAction(_ sender: UITapGestureRecognizer) {
        let touchPoint = sender.location(in: self.mainImageView)
        let Z1:String = String(describing: touchPoint.x)
        let Z2:String = String(describing: touchPoint.y)
        print( "YES It Works X= " + Z1 + " Y= " + Z2)
    }
}
