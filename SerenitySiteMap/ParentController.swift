//
//  ParentController.swift
//  SerenitySiteMap
//
//  Created by Ilya on 02/09/16.
//  Copyright © 2016 DDG LLC. All rights reserved.
//
import UIKit
class ParentController: UIViewController, UIPageViewControllerDataSource {
    
    
    public var identifier: String?
    // MARK: - Variables
    fileprivate var pageViewController: UIPageViewController?
    
    fileprivate var timer: Timer?
    
    // Initialize it right away here
    fileprivate var contentImages1 = ["Gal1",
                                      "Gal2",
                                      "Gal3",
                                      "Gal4",
                                      "Gal5",
                                      "Gal6",]
    
    fileprivate var contentImages2 = ["Amen1",
                                      "Amen2",
                                      "Amen3",
                                      "Amen4",
                                      "Amen5",
                                      "Amen6",
                                      "Amen7",
                                      "Amen8",
                                      "Amen9",
                                      "Amen10",
                                      "Amen11",
                                      "Amen12",
                                      "Amen13"]
    
    fileprivate var contentImages3 = ["SandPiperGal",
                                      "VoyagerGal",
                                      "ClipperGal",
                                      "MarinerGal",
                                      "CaspianGal"]
    
    fileprivate var contentImages: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch identifier!{
            
        case "Amenities":
            contentImages = contentImages2
            
        case "NewGallery":
            contentImages = contentImages3
            
        default:
            contentImages = contentImages1
        }
        
        createPageViewController()
        setupPageControl()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func tapAction() {
        timer?.invalidate()
        if !(self.navigationController?.isNavigationBarHidden)! {
            hideBars()
            return
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        for view in (self.pageViewController?.view.subviews)! {
//            if ((view as? UIPageControl) != nil) {
//                view.isHidden = false
//            }
//        }
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(hideBars), userInfo: nil, repeats: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(hideBars), userInfo: nil, repeats: false)
    }
    
    func hideBars () {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
//        for view in (self.pageViewController?.view.subviews)! {
//            if ((view as? UIPageControl) != nil) {
//                view.isHidden = true
//            }
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        timer?.invalidate()
    }
    
    fileprivate func createPageViewController() {
        
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers = [firstController]
            pageController.setViewControllers(startingViewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParentViewController: self)
    }
    
    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
        appearance.backgroundColor = UIColor.darkGray
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        
        if itemController.itemIndex > 0 {
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        
        if itemController.itemIndex+1 < contentImages.count {
            return getItemController(itemController.itemIndex+1)
        }
        
        return nil
    }
    
    fileprivate func getItemController(_ itemIndex: Int) -> PageItemController? {
        
        if itemIndex < contentImages.count {
            let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "ItemController") as! PageItemController
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            return pageItemController
        }
        
        return nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
}
