//
//  PageItemController.swift
//  SerenitySiteMap
//
//  Created by Ilya on 02/09/16.
//  Copyright © 2016 DDG LLC. All rights reserved.
//

import Foundation
import UIKit
class PageItemController: UIViewController, UIScrollViewDelegate {
    
    var itemIndex: Int = 0
    var imageName: String = "" {
        
        didSet {
            
            if let imageView = image {
                imageView.image = UIImage(named: imageName)
            }
            
        }
    }
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var scrollVIew: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image!.image = UIImage(named: imageName)
        self.scrollVIew.minimumZoomScale = 1.0
        self.scrollVIew.maximumZoomScale = 3.0
        self.scrollVIew.delegate = self
        self.scrollVIew.contentSize = CGSize(width: scrollVIew.contentSize.width, height: 1.0)
       
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.image
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if (scrollView.zoomScale < 1) {
            scrollView.zoomScale = 1
        }
    }
    
  //  override func viewDidLayoutSubviews() {
//        if !UIDevice.current.orientation.isLandscape {
//            self.image.contentMode = UIViewContentMode.scaleAspectFit
//
//        } else {
//            self.image.contentMode = UIViewContentMode.scaleAspectFill
//
//        }

 //   }

}
